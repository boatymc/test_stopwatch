package com.example.shann.stopwatch;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


   private TextView mTvTime;
   private Button mBtnStart;
   private Button mBtnLap;
   private Button mBtnStop;

   private Context mContext;
   private Chronometer mChronometer;
   private Thread mThreadChrono;

   public boolean haltDisplayForLap= false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        mTvTime = findViewById(R.id.tv_time);
        mBtnStart = findViewById(R.id.btn_start);
        mBtnLap = findViewById(R.id.btn_lap);
        mBtnStop = findViewById(R.id.btn_stop);

        //btn_start click handler
        mBtnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if chronometer has not been instantiated before...
                if(mChronometer == null){
                    //instantiated the chronometer
                    mChronometer = new Chronometer(mContext);
                    //run the chronometer on a separate thread
                    mThreadChrono = new Thread(mChronometer);
                    mThreadChrono.start();

                    //start the chronometer
                    mChronometer.start();
                }
            }
        });

        mBtnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the chronometer had been instantiated before
                if(mChronometer != null){
                    //stop the chronometer
                    mChronometer.stop();
                    //stop the thread
                    mThreadChrono.interrupt();
                    mThreadChrono = null;
                    //kill the chrono class
                    mChronometer = null;
                }else{
                    String resetTimeText= "00:00:00:000";
                    //TODO: Integer.toString(R.string.timerStart);
                    // fix this reference - it is returning an int and the parse is helping
                    updateTimerText(resetTimeText);
                }
            }
        });

        mBtnLap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the chronometer had been instantiated before
                if(mChronometer != null){
                    //toggle lap display
                    haltDisplayForLap = !haltDisplayForLap;

                }
            }
        });
    }


    public void updateTimerText(final String timeAsText) {

        if(!haltDisplayForLap){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTvTime.setText(timeAsText);
                }
            });
        }

        }
    }

